package io.gitlab.booster.example.fulfillment.config;

import arrow.core.Either;
import arrow.core.Option;
import com.google.cloud.spring.pubsub.core.PubSubTemplate;
import com.google.cloud.spring.pubsub.support.AcknowledgeablePubsubMessage;
import com.google.pubsub.v1.PubsubMessage;
import io.gitlab.booster.commons.metrics.MetricsRegistry;
import io.gitlab.booster.config.thread.ThreadPoolConfig;
import io.gitlab.booster.config.thread.ThreadPoolSetting;
import io.gitlab.booster.messaging.config.GcpPubSubSubscriberConfig;
import io.gitlab.booster.messaging.config.GcpPubSubSubscriberSetting;
import io.gitlab.booster.messaging.config.OpenTelemetryConfig;
import io.gitlab.booster.messaging.publisher.PublisherRecord;
import io.gitlab.booster.messaging.publisher.gcp.GcpPubSubPublisher;
import io.gitlab.booster.messaging.publisher.gcp.PubsubRecord;
import io.gitlab.booster.messaging.subscriber.gcp.GcpPubSubPullSubscriber;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PubSubEmulatorContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import reactor.test.StepVerifier;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.notNullValue;


@Testcontainers
@ExtendWith(SpringExtension.class)
@SpringBootTest(
        classes = {
                GcpPubsubConfig.class
        },
        properties = {
                "booster.gcp.pubsub.create=true"
        }
)
@EnableAutoConfiguration
@DirtiesContext
class GcpPubsubConfigTest {

    private static final Logger log = LoggerFactory.getLogger(GcpPubsubConfigTest.class);

    @Autowired
    private PubSubTemplate template;

    @Autowired
    private ConfigurableApplicationContext context;

    @Autowired
    private GcpPubsubConfig pubsubConfig;

    @Container
    public static PubSubEmulatorContainer emulator = new PubSubEmulatorContainer(
            DockerImageName.parse("gcr.io/google.com/cloudsdktool/google-cloud-cli:380.0.0-emulators")
                    .asCompatibleSubstituteFor("gcr.io/google.com/cloudsdktool/cloud-sdk")
    );

    @DynamicPropertySource
    static void registerProperties(DynamicPropertyRegistry registry) {
        registry.add("booster.gcp.pubsub.endpoint", () -> emulator.getEmulatorEndpoint());
    }

    private void verifyPublishResult(Either<Throwable, Option<PublisherRecord>> result) {
        assertThat(result, instanceOf(Either.Right.class));
        assertThat(result.getOrNull(), notNullValue());
        assertThat(result.getOrNull().isDefined(), equalTo(true));
        assertThat(result.getOrNull().orNull(), notNullValue());
        assertThat(result.getOrNull().orNull().getRecordId(), notNullValue());
    }

    private void publish(GcpPubSubPublisher<String> publisher) {
        for (int i = 0; i < 10; i++) {
            StepVerifier.create(publisher.publish("orders", new PubsubRecord<>(Integer.toString(i))))
                    .consumeNextWith(this::verifyPublishResult)
                    .verifyComplete();
        }
    }

    @Test
    void shouldReceive() {
        ThreadPoolConfig threadPoolConfig = new ThreadPoolConfig(this.context, new MetricsRegistry(new SimpleMeterRegistry()));
        ThreadPoolSetting threadPoolSetting = new ThreadPoolSetting();
        threadPoolConfig.setSettings(Map.of("test", threadPoolSetting, "publisher", new ThreadPoolSetting() ));

        GcpPubSubSubscriberConfig subscriberConfig = new GcpPubSubSubscriberConfig();
        GcpPubSubSubscriberSetting subSubscriberSetting = new GcpPubSubSubscriberSetting();
        subSubscriberSetting.setMaxRecords(10);
        subSubscriberSetting.setSubscription("order-subscription");

        GcpPubSubPublisher<String> publisher = new GcpPubSubPublisher<>(
                "publisher",
                this.template.getPubSubPublisherTemplate(),
                threadPoolConfig,
                new MetricsRegistry(new SimpleMeterRegistry()),
                new OpenTelemetryConfig(null, "test"),
                true
        );

        this.publish(publisher);

        subscriberConfig.setSettings(Map.of("test", subSubscriberSetting));

        GcpPubSubPullSubscriber subscriber = new GcpPubSubPullSubscriber(
                "test",
                this.template.getPubSubSubscriberTemplate(),
                threadPoolConfig,
                subscriberConfig,
                new MetricsRegistry(new SimpleMeterRegistry()),
                new OpenTelemetryConfig(null, "test"),
                true
        );

        StepVerifier.create(subscriber.flatFlux().take(5))
                .consumeNextWith(message -> checkMessage(message, 0L) )
                .consumeNextWith(message -> checkMessage(message, 1L) )
                .consumeNextWith(message -> checkMessage(message, 2L) )
                .consumeNextWith(message -> checkMessage(message, 3L) )
                .consumeNextWith(message -> checkMessage(message, 4L) )
                .verifyComplete();
    }

    private void checkMessage(AcknowledgeablePubsubMessage message, long value) {
        assertThat(message.getPubsubMessage(), notNullValue());
        PubsubMessage msg = message.getPubsubMessage();
        String content = msg.getData().toString(StandardCharsets.UTF_8);
        assertThat(content, notNullValue());
        assertThat(Long.parseLong(content), equalTo(value));
        message.ack();
    }

    @Test
    void shouldSend() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        AtomicBoolean published = new AtomicBoolean(false);
        CompletableFuture<String> result = template.publish(
                this.pubsubConfig.getTopic(),
                "abc"
        );

        result.whenCompleteAsync((str, t) -> {
            if (t != null) {
                log.error("message publish to pub/sub failed", t);
            } else {
                log.info("message published to pub/sub with result: {}", result);
                published.set(true);
                latch.countDown();
            }
            latch.countDown();
        });

        latch.await(2L, TimeUnit.SECONDS);
        assertThat(published.get(), equalTo(true));

        List<AcknowledgeablePubsubMessage> messages = this.template.pull(
                this.pubsubConfig.getSubscription(),
                10,
                true
        );
        assertThat(messages, hasSize(1));
        String msg = messages.getFirst().getPubsubMessage().getData().toString(StandardCharsets.UTF_8);
        log.info("message received: {}", msg);
        assertThat(msg, equalTo("abc"));
    }
}
